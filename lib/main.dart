import 'package:flutter/material.dart';
import 'package:tutorial_screens/wrapped_screens.dart';
import 'package:tutorial_screens/tutorial_screens/Page1.dart';
import 'package:tutorial_screens/tutorial_screens/Page2.dart';
import 'package:tutorial_screens/tutorial_screens/Page3.dart';
import 'package:tutorial_screens/tutorial_screens/Page4.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  MyApp({super.key});

  List<Widget> tutorialPagesList = [
    const Page1(),
    const Page2(),
    const Page3(),
    const Page4()
  ];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Tutorial Screens',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: WrappedScreens(tutorialPagesList),
    );
  }
}
