import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'home_page.dart';

class WrappedScreens extends StatefulWidget {
  final List<Widget> tutorialPagesList;

  const WrappedScreens(this.tutorialPagesList, {Key? key}) : super(key: key);

  @override
  _WrappedScreensState createState() => _WrappedScreensState();
}

class _WrappedScreensState extends State<WrappedScreens> {
  final PageController _controller = PageController();
  bool onLastPage = false;

  @override
  Widget build(BuildContext context) {
    int numberOfPages = widget.tutorialPagesList.length;

    return Scaffold(
      body: Stack(children: [
        PageView(
            onPageChanged: (index) {
              setState(() {
                onLastPage = (index == numberOfPages - 1);
              });
            },
            controller: _controller,
            children: widget.tutorialPagesList),
        Container(
            alignment: const Alignment(0, 0.9),
            child: Column(
              children: [
                const SizedBox(height: 80),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(
                      height: 80,
                      width: 80,
                      child: Image.network(
                        "https://triviacrack.com/resources/images/cards/games/Trivia%20crack@2x.png",
                      ),
                    ),
                    Text(
                      'TRIVIA CRACK',
                      style: GoogleFonts.secularOne(
                          textStyle: Theme.of(context).textTheme.headlineLarge,
                          fontWeight: FontWeight.bold,
                          color: Colors.black87),
                    ),
                  ],
                ),
                const Spacer(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    GestureDetector(
                      child: const Text("prev"),
                      onTap: () {
                        _controller.previousPage(
                          duration: const Duration(milliseconds: 400),
                          curve: Curves.easeIn,
                        );
                      },
                    ),
                    SmoothPageIndicator(
                        effect: const WormEffect(activeDotColor: Colors.blue),
                        controller: _controller,
                        count: numberOfPages),
                    onLastPage
                        ? GestureDetector(
                            child: const Text(
                              "done",
                            ),
                            onTap: () {
                              Navigator.push(context,
                                  MaterialPageRoute(builder: (context) {
                                return const HomePage();
                              }));
                            },
                          )
                        : GestureDetector(
                            child: const Text("next"),
                            onTap: () {
                              _controller.nextPage(
                                duration: const Duration(milliseconds: 400),
                                curve: Curves.easeIn,
                              );
                            },
                          ),
                  ],
                ),
                const SizedBox(height: 30),
                ElevatedButton.icon(
                  icon: Icon(
                    Icons.share,
                  ),
                  label: const Text(
                    'SHARE',
                    style: TextStyle(fontSize: 20),
                  ),
                  style: OutlinedButton.styleFrom(
                    backgroundColor: const Color(0xff1bc4b1),
                    fixedSize: const Size(300, 50),
                    shape: const RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(6))),
                  ),
                  onPressed: () {
                    print('Pressed');
                  },
                ),
                const SizedBox(height: 30)
              ],
            ))
      ]),
    );
  }
}
