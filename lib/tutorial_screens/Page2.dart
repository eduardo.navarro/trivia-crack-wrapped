import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:lottie/lottie.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';

class Page2 extends StatelessWidget {
  const Page2({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(top: 80, left: 30, right: 30),
      color: Colors.white70,
      child: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            const SizedBox(
              height: 80,
            ),
            const SizedBox(
              height: 20,
            ),
            const Text(
              '@triviacrackuser',
              style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                  color: Colors.black54),
            ),
            const Text(
              textAlign: TextAlign.center,
              "2022 stats &\n 2023 fortune",
              style: TextStyle(
                  fontSize: 42,
                  fontWeight: FontWeight.w600,
                  color: Colors.black54),
            ),
            Stack(
              alignment: Alignment.center,
              children: [
                SizedBox(
                  width: double.infinity,
                  height: 230,
                  child: Lottie.network(
                      'https://assets10.lottiefiles.com/packages/lf20_tiviyc3p.json',
                      fit: BoxFit.cover),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 20),
                  child: CircularPercentIndicator(
                    radius: 90.0,
                    lineWidth: 8.0,
                    animation: true,
                    animationDuration: 1000,
                    percent: 0.89,
                    center: const Text(
                      textAlign: TextAlign.center,
                      "89%",
                      style:
                          TextStyle(fontSize: 64, fontWeight: FontWeight.bold),
                    ),
                    circularStrokeCap: CircularStrokeCap.round,
                    progressColor: Colors.blue,
                  ),
                ),
              ],
            ),
            const Text(
              textAlign: TextAlign.center,
              "Win matches!",
              style: TextStyle(fontSize: 30, fontWeight: FontWeight.w400),
            ),
            const SizedBox(
              height: 30,
            ),
            const Text(
              textAlign: TextAlign.start,
              "“2023 finds you on a winning streak. It wasn't easy and you needed power ups more than once. But you can do it, your dreams are only 11% ahead”",
              style: TextStyle(fontSize: 26, color: Colors.black87),
            ),
          ],
        ),
      ),
    );
  }
}
