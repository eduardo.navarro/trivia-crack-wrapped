import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:lottie/lottie.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';

class Page3 extends StatelessWidget {
  const Page3({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(top: 80, left: 30, right: 30),
      color: Colors.white70,
      child: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            const SizedBox(
              height: 80,
            ),
            const SizedBox(
              height: 20,
            ),
            const Text(
              '@triviacrackuser',
              style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                  color: Colors.black54),
            ),
            const Text(
              textAlign: TextAlign.center,
              "2022 stats &\n 2023 fortune",
              style: TextStyle(
                  fontSize: 42,
                  fontWeight: FontWeight.w600,
                  color: Colors.black54),
            ),
            const SizedBox(
              height: 30,
            ),
            Column(children: [
              const Text(
                textAlign: TextAlign.center,
                "You are expert in:",
                style: TextStyle(fontSize: 30, fontWeight: FontWeight.w400),
              ),
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Lottie.network(
                        'https://assets7.lottiefiles.com/packages/lf20_ky03n5aXvs.json',
                        height: 130,
                        width: 130,
                        fit: BoxFit.cover),
                    const SizedBox(
                      width: 30,
                    ),
                    Padding(
                      padding: const EdgeInsets.all(14.0),
                      child: Column(
                        children: const [
                          Text(
                            textAlign: TextAlign.center,
                            "SPORTS",
                            style: TextStyle(
                                fontSize: 38,
                                fontWeight: FontWeight.bold,
                                letterSpacing: -1),
                          ),
                          Text(
                            textAlign: TextAlign.center,
                            "89%",
                            style: TextStyle(
                                fontSize: 64, fontWeight: FontWeight.bold),
                          ),
                          Text(
                            textAlign: TextAlign.center,
                            "Right answers!",
                            style: TextStyle(
                                fontSize: 26, fontWeight: FontWeight.w400),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ]),
            const SizedBox(
              height: 60,
            ),
            const Text(
              textAlign: TextAlign.start,
              "“Messi would be proud of you!\n\nKeep training, keep sharp, 2023 will find you striking your best shot!”",
              style: TextStyle(fontSize: 24, color: Colors.black87),
            ),
          ],
        ),
      ),
    );
  }
}
