import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:lottie/lottie.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';

class Page4 extends StatelessWidget {
  const Page4({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(top: 80, left: 30, right: 30),
      color: Colors.white70,
      child: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            const SizedBox(
              height: 80,
            ),
            const SizedBox(
              height: 20,
            ),
            const Text(
              '@triviacrackuser',
              style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                  color: Colors.black54),
            ),
            const Text(
              textAlign: TextAlign.center,
              "2022 stats &\n 2023 fortune",
              style: TextStyle(
                  fontSize: 42,
                  fontWeight: FontWeight.w600,
                  color: Colors.black54),
            ),
            const SizedBox(
              height: 40,
            ),
            Column(
              children: const [
                Text(
                  textAlign: TextAlign.center,
                  "Final Score",
                  style: TextStyle(
                    fontSize: 36,
                    fontWeight: FontWeight.w400,
                    letterSpacing: -1,
                  ),
                ),
                Text(
                  textAlign: TextAlign.center,
                  "76%",
                  style: TextStyle(fontSize: 64, fontWeight: FontWeight.bold),
                ),
                Text(
                  textAlign: TextAlign.center,
                  "Right answers!",
                  style: TextStyle(fontSize: 30, fontWeight: FontWeight.w400),
                ),
                Text(
                  textAlign: TextAlign.center,
                  "Superaste a 3,567,000 personas!",
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.w400),
                ),
              ],
            ),
            const SizedBox(
              height: 50,
            ),
            const Text(
              textAlign: TextAlign.start,
              "“Awsome! Your 2022 was a tough match but you ended as a real champion. 2023 will be xxxxxxxx xxx for you!”",
              style: TextStyle(fontSize: 26, color: Colors.black87),
            ),
          ],
        ),
      ),
    );
  }
}
